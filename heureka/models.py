from django.dispatch import receiver
from django.db import models
from django.db.models.signals import pre_save
from django.utils.translation import ugettext_lazy as _

from mezzanine.core.managers import CurrentSiteManager
from mezzanine.core.models import SiteRelated
from mezzanine.utils.sites import current_site_id

from cartridge.shop.models import Product, ProductOption, ProductVariation


class HeurekaUpdateManager(CurrentSiteManager):

    def update_heureka(self):
        site_id = current_site_id()
        heureka_update, created = self.get_or_create(site_id=site_id)
        if not created:
            heureka_update.save()

    def get_last_update(self):
        site_id = current_site_id()
        heureka_update, created = self.get_or_create(site_id=site_id)
        return heureka_update.last_update


class HeurekaUpdate(SiteRelated):
    last_update = models.DateTimeField(_('Last update'), auto_now=True,)

    objects = HeurekaUpdateManager()

    class Meta:
        verbose_name = _('Heureka update')
        verbose_name_plural = _('Heureka update')


class HeurekaRequestManager(CurrentSiteManager):

    def log_access(self):
        self.create()


class HeurekaRequest(SiteRelated):
    last_request = models.DateTimeField(_('Last request'), auto_now_add=True,)

    objects = HeurekaRequestManager()

    class Meta:
        verbose_name = _('Heureka request')
        verbose_name_plural = _('Heureka requests')


@receiver(pre_save, sender=Product)
@receiver(pre_save, sender=ProductVariation)
def update_heureka(sender, instance, *args, **kwargs):
    HeurekaUpdate.objects.update_heureka()