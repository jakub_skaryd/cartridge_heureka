from django.utils.translation import ugettext_lazy as _

from mezzanine.conf import register_setting


register_setting(
    name="EXTRA_MODEL_FIELDS",
    editable=False,
    default=(
        (
            "cartridge.shop.models.Product.heureka_name",
            'CharField',
            ("Heureka Name",),
            {"max_length": 255, "blank": True, "null": True},
        ),
        (
            "cartridge.shop.models.ProductVariation.heureka_appendix",
            'CharField',
            ("Heureka Appendix"),
            {"max_length": 255, "blank": True, "null": True},
        ),
    ),
    append=True,
)


register_setting(
    name="ADMIN_MENU_ORDER",
    editable=False,
    default=(
        ('Heureka', ('heureka.HeurekaUpdate', 'heureka.HeurekaRequest', )),
    ),
    append=True,
)
