from django.conf.urls import patterns,  url


urlpatterns = patterns('',
    url(r"^", "heureka.views.heureka_export", name="heureka_export")
)
