from os.path import join

from django.conf import settings
from django.views.decorators.http import condition, HttpResponse

import xml.dom.minidom

from xml.etree import ElementTree as ET

from cartridge.shop.models import Product, Category, ProductImage, ProductVariation, ProductOption

from heureka.models import HeurekaUpdate, HeurekaRequest


EL_SHOP = 'SHOP'
EL_SHOPITEM = 'SHOPITEM'
EL_ITEM_ID = 'ITEM_ID'
EL_PRODUCTNAME = 'PRODUCTNAME'
EL_PRODUCT = 'PRODUCT'
EL_DESCRIPTION = 'DESCRIPTION'
EL_URL = 'URL'
EL_IMGURL = 'IMGURL'


def create_xml(request, products):
    shop = ET.Element(EL_SHOP)
    for product in products:
        for variation in product.variations.all():
            shop_item = ET.Element(EL_SHOPITEM)
            shop.append(shop_item)

            item_id = ET.Element(EL_ITEM_ID)
            item_id.text = variation.sku

            description = ET.Element(EL_DESCRIPTION)
            description.text = product.description

            url = ET.Element(EL_URL)
            url.text = product.get_absolute_url()
            url.text = request.build_absolute_uri(url.text)

            img_url = ET.Element(EL_IMGURL)
            img_url.text = join(settings.MEDIA_URL, product.image)
            img_url.text = request.build_absolute_uri(img_url.text)

            shop_item.append(item_id)
            shop_item.append(description)
            shop_item.append(url)
            shop_item.append(img_url)

    return shop


def lasted_update(request):
    return HeurekaUpdate.objects.get_last_update()


@condition(last_modified_func=lasted_update)
def heureka_export(request):
    products = Product.objects.filter(available=True)
    xmlx = create_xml(request, products)
    xml_str = ET.tostring(xmlx, encoding='utf-8')
    final_xml = '<?xml version="1.0" encoding="UTF-8"?>%s' % xml_str

    HeurekaRequest.objects.log_access()
    return HttpResponse(final_xml, content_type="text/xml")


if __name__ == '__main__':
    import django
    from django.http import HttpRequest
    request = HttpRequest()
    request.META["SERVER_NAME"] = 'eshop-adidas.cz'
    request.META["SERVER_PORT"] = 80
    django.setup()
    products = Product.objects.all()
    xmlx = create_xml(request, products)

    xml_str = ET.tostring(xmlx, encoding='utf-8')
    print xml_str
    xml1 = xml.dom.minidom.parseString(xml_str) # or xml.dom.minidom.parseString(xml_string)
    pretty_xml_as_string = xml1.toprettyxml()
    print pretty_xml_as_string