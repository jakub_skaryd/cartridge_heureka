from copy import deepcopy

from django.contrib import admin

from mezzanine.core.admin import SingletonAdmin

from cartridge.shop.admin import ProductAdmin, ProductVariationAdmin, ProductImageAdmin
from cartridge.shop.models import Product, ProductVariation
from heureka.models import HeurekaUpdate, HeurekaRequest


product_variation_fieldsets = deepcopy(ProductVariationAdmin.fields)
product_variation_fieldsets.insert(-1, "heureka_appendix")
class ProductVariationHeurekaAdmin(ProductVariationAdmin):
    fields = product_variation_fieldsets


product_fieldsets = deepcopy(ProductAdmin.fieldsets)
product_fieldsets[0][1]["fields"].insert(2, "heureka_name")
class ProductHeurekaAdmin(ProductAdmin):
    fieldsets = product_fieldsets
    inlines = (ProductImageAdmin, ProductVariationHeurekaAdmin)
    # prepopulated_fields = {"heureka_name": ("title",), }

class HeurekaUpdateAdmin(SingletonAdmin):
    readonly_fields = ('last_update',)
    list_display = ('last_update',)


class HeurekaRequestAdmin(admin.ModelAdmin):
    readonly_fields = ('last_request',)
    list_display = ('last_request',)

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return True

    def has_delete_permission(self, request, obj=None):
        return False

    def has_module_permission(self, request):
        return True


admin.site.register(HeurekaUpdate, HeurekaUpdateAdmin)
admin.site.register(HeurekaRequest, HeurekaRequestAdmin)
admin.site.unregister(Product)
admin.site.register(Product, ProductHeurekaAdmin)