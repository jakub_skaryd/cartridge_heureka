from setuptools import setup, find_packages

from heureka import __version_str__

setup(
    name='heureka',
    version=__version_str__,
    packages=find_packages(),
    url='',
    license='BSD',
    author='MT Production s.r.o',
    author_email='jakub.skaryd@mtproduction.cz',
    description='',
    install_requires=(
        'mezzanine',
    )
)
